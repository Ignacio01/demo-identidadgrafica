# Pruebacolor

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Libraries used for the creation of this demo.

* ng add @angular/material

Modules imported into app.module

* import {MatButtonModule} from '@angular/material/button';
* import {MatFormFieldModule} from '@angular/material/form-field';
* import {MatMenuModule} from '@angular/material/menu';
* import {ReactiveFormsModule} from '@angular/forms';
* import {FormsModule} from '@angular/forms';

imports: [
    ...,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatMenuModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ]


## To-Do

* [x] Creation of a service that consumes a database in postgres
* [x] Create a component called identity.
* [x] Define the structure of the html in the component using angular material
* [x] Define a double link structure between the component.ts and the html using the material libraries
* [x] Connect to a service that in turn consumes values ​​from a database in postgres


## Running the application locally
This application is connected to a local service developed in Nodejs, since it is a Demo, the only thing that was sought with this connection is to determine if a graphic identity of a system can be made through dynamic styles in the html interface.
Therefore, the tests carried out, although basic, have shown that if it is possible to make a graphic identity of a system in a dynamic way.

## Description of the application

* [x] To initialize the application it is necessary to start the database in postgres.
* [x] Start the Nodejs service
* [x] Start the Angular project

The default path that was defined for this demo is "http: // localhost: 4200 / identity".


## Aggregates.
The ResApi service made in Nodejs has been included in a file

###Package structure

## Files and Directories

```text
.
├── demo-maps
├── src
│   └── app
│       └── identidad
│       	├──modelo
│		│	└──identidad.ts
│           	├── service
│		│	└──servicecolors.service.ts
│		│	└──servicecolors.service.spec.ts
│		│
│	        ├── identidad.component.html
│	        ├── identidad.component.css
│	        ├── identidad.component.spec.ts
│           	└── identidad.component.ts
│           
│           
├── src
│   └── app
│		├── app.component.css
│	    ├── app.component.html
│	    ├── app.component.spec.ts
│       └── app.component.ts
│       └── app.module.ts
│       └── app-routing.module.ts
│
├── src
│   └── app
│   └── assets
│   └── environments
│	├── favicon.ico
│	├── index.html
│	├── main.ts
│   └── polyfills.ts
│   └── styles.css
│   └── test.ts
│
│
├── .browserslistrc
├── .editorconfig
├── .gitignore
├── angular
├── karma.conf
├── package
├── package-lock
├── README
├── tsconfig.app
├── tsconfig
├── tsconfig.spec
├── tslint
```

