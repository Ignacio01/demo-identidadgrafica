import { Component, OnInit } from '@angular/core';
import { identidad } from './modelo/identidad';
import { ServicecolorsService } from './service/servicecolors.service';

@Component({
  selector: 'app-identidad',
  templateUrl: './identidad.component.html',
  styleUrls: ['./identidad.component.css']
})
export class IdentidadComponent implements OnInit {

  colorPrimario: string = 'gray'
  colorSecundario = 'gray'
  colorTerciario = 'gray'
  imagen = ''

  ///
  public primario: string ="gray"
  public secundario: string ="gray"
  public terciario: string ="gray"
  public imagenNueva: string = ""

  identidad: any =[]
  nuevaidentidad: identidad={
    id: 1,
    primario: "",
    secundario:  "",
    terciario:  "",
    urlImagen:  ""
  }
  constructor(private serviceIdentidad: ServicecolorsService) { }

  ngOnInit(): void {
    this.serviceIdentidad.getIdentidad().subscribe(
      data=>{
        console.log(data)
        this.identidad = data[0]
        this.primario = this.identidad['primario']
        this.secundario = this.identidad['secundario']
        this.terciario = this.identidad['terciario']
        this.imagenNueva = this.identidad['urlimagen']
      }
    )
  }

  
  getIdentidad(){
    this.serviceIdentidad.getIdentidad().subscribe(
      data=>{
        this.identidad = data[0]
        this.colorPrimario = this.identidad['primario']
        this.colorSecundario = this.identidad['secundario']
        this.colorTerciario = this.identidad['terciario']
        this.imagen = this.identidad['urlimagen']
        this.imagenNueva = this.identidad['urlimagen']

      }
    )
  }

  CambiarColor(){
    this.nuevaidentidad.primario = this.primario
    this.nuevaidentidad.secundario = this.secundario
    this.nuevaidentidad.terciario = this.terciario
    this.nuevaidentidad.urlImagen = this.imagenNueva

    this.serviceIdentidad.updateIdentidad(this.nuevaidentidad).subscribe(
      data=>{
        console.log(data)
      }
    )
    this.getIdentidad()
  }
  
}
