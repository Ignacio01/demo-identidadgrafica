import { TestBed } from '@angular/core/testing';

import { ServicecolorsService } from './servicecolors.service';

describe('ServicecolorsService', () => {
  let service: ServicecolorsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServicecolorsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
