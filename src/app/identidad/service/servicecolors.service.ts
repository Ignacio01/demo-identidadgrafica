import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { identidad } from '../modelo/identidad';

@Injectable({
  providedIn: 'root'
})
export class ServicecolorsService {

  API_URI = "http://localhost:3000";
  constructor(private http: HttpClient) { }

  getIdentidad(){
    return this.http.get(`${this.API_URI}/identidad`);
  }

  updateIdentidad(identidad: identidad ){
    return this.http.put(`${this.API_URI}/identidad/1`, identidad)
  }
}
