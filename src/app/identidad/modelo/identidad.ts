export interface identidad{
    id?: number,
    primario?: string,
    secundario?: string,
    terciario?: string,
    urlImagen?: string
}