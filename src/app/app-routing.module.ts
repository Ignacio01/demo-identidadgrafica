import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IdentidadComponent } from './identidad/identidad.component';

const routes: Routes = [
  {
    path: 'colors',
    component: IdentidadComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
